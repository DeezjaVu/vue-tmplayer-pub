"use strict";

import Store from "electron-store";

/**
 * The data in this file is initial (default) data.
 * 
 * The actual app data is stored in the user's AppData/Roaming directory, 
 * in a folder with the same name as the application as defined in the package.json config.
 * 
 * For this app the path is:"%AppData%\TMPlayer\config.json"
 */
export default new Store({
	defaults: {
		about: {
			author: "Peter Ginneberge",
			email: "p.ginneberge@gmail.com"
		},
		tmp: {
			token: "",
			user: {}
		}
	}
});
