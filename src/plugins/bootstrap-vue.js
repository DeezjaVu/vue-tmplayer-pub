import Vue from "vue";

import BootstrapVue from 'bootstrap-vue';
// import 'bootstrap/dist/css/bootstrap.min.css'
import "bootswatch/dist/darkly/bootstrap.min.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

Vue.use(BootstrapVue);
