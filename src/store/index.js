"use strict";

import Vue from "vue";
import Vuex from "vuex";
import User from "./models/user.model";
import Streams from "./models/streams.model";
import Channels from "./models/channels.model";
import Games from "./models/games.model";

Vue.use(Vuex);

// TODO: set correct interval for live streams timer.
// const TIMER_INTERVAL = 1000 * 60;
const TIMER_INTERVAL = 1000 * 60 * 5;

export default new Vuex.Store({
  modules: {
    userModel: User,
    streamsModel: Streams,
    channelsModel: Channels,
    gamesModel: Games
  },
  state: {
    liveStreamInterval: -1
  },
  mutations: {

  },
  actions: {
    /**
     * 
     * @param {*} state Reference to the Store"s state.
     * @param {*} token The Twitch OAuth token to validate.
     */
    setOAuthToken({ state }, token) {
      console.log("Store ::: setOAuthToken");
      console.log(" - token:", token);
      console.log(" - ignore this:", state.liveStreamInterval);

    },
    /**
     * 
     * @param {*} dispatch Used to dispatch events on Store.
     * @param {*} state Reference to the Store"s state.
     */
    startLiveStreams({ dispatch, state }) {
      console.log("Store ::: actions ::: startLiveStreams");
      const startTime = Date.now();
      console.log(" - START TIME:", startTime);

      state.liveStreamInterval = setInterval(async () => {
        console.log("Store ::: startLiveStreams ::: interval");
        console.log(" - INTERVAL TIME:", Math.floor((Date.now() - startTime) / 1000), "seconds");
        dispatch("getLiveStreams").then(() => {
          console.log("Store ::: getLiveStreams ::: then");
        });

      }, TIMER_INTERVAL);
      console.log(" - interval:", state.liveStreamInterval);
    },
    /**
     * 
     * @param {*} state Reference to the Store state.
     */
    stopLiveStreams({ state }) {
      console.log("Store ::: actions ::: stopLiveStreams");
      clearInterval(state.liveStreamInterval);
      console.log(" - interval:", state.liveStreamInterval);
    }
  },
  getters: {
    /**
     * 
     * @param {*} state 
     * @returns A list of live streams for the authenticated user.
     */
    streams(state) {
      return state.streamsModel.streams;
    },
    /**
     * Getter for StreamsModel newStreams.
     * @param {*} state Store state.
     * @returns {array} A list (Array) of new live streams.
     */
    newStreams(state) {
      return state.streamsModel.newStreams;
    },
    /**
     * 
     * @param {*} state 
     * @returns {array} A list of twitch channels.
     */
    channels(state) {
      return state.channelsModel.channels;
    },
    followedUsers(state) {
      return state.userModel.followedUsers;
    }
  }
});
