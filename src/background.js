"use strict";

import { app, BrowserWindow, ipcMain, Notification, protocol, shell } from "electron";
import path from "path";
// import {  createProtocol,  installVueDevtools} from "vue-cli-plugin-electron-builder/lib";
import { createProtocol } from "vue-cli-plugin-electron-builder/lib";

/**
 * LOCAL MODULES
 */
// const config = require("./config/tmp-config");
import config from "./config/tmp-config.js";
import subWindows from "./windows/sub-windows.js";
import twitchApi from "./twitch/twitch-api.js";

const isDevelopment = process.env.NODE_ENV !== "production";

// Keep a global reference of the window object, if you don"t, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

// Scheme must be registered before the app is ready
protocol.registerSchemesAsPrivileged([{ scheme: "app", privileges: { secure: true, standard: true } }]);

console.log("WEBPACK_DEV_SERVER_URL:", process.env.WEBPACK_DEV_SERVER_URL);
console.log("TWITCH_CLIENT_ID:", process.env.VUE_APP_TWITCH_CLIENT_ID);
// for (const key in process.env) {
//   if (process.env.hasOwnProperty(key) && key.indexOf("TWITCH") !== -1) {
//     console.log(`${key}:`, process.env[key]);
//   }
// }

// nativeTheme.on("updated", function theThemeHasChanged() {
//   console.log("MAIN ::: nativeTheme ::: updated");
//   console.log(" - use dark colors:", nativeTheme.shouldUseDarkColors);
// });

// nativeTheme.themeShource = "dark";

const { systemPreferences } = require("electron");
console.log(" - SYSTEM DARK MODE:", systemPreferences.isDarkMode());

/**
 * 
 */
function createMainWindow() {
  console.log("MAIN ::: createMainWindow");

  // 1150x720
  const winArgs = {
    width: 1166,
    height: 758,
    minWidth: 1166,
    minHeight: 778,
    // minHeight: 816,
    show: false,
    icon: path.join(__static, "icon.png"),
    webPreferences: {
      // preload: path.join(__static, "js", "preload.js"),
      nodeIntegration: true
    }
  };
  // Create the browser window.
  mainWindow = new BrowserWindow(winArgs);

  mainWindow.setMenuBarVisibility(false);

  console.log(" - __static:", __static);
  console.log(" - __dirname:", __dirname);
  // console.log(" - icon path:", path.join(__dirname, "/bundled/icons/png/256x256.png"));

  // INFO: app icon is set at window creation.
  // Using electron-icon-builder in package.json to generate icons
  // mainWindow.setIcon(path.join(__dirname, "/bundled/icons/png/256x256.png"));

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    // Load the url of the dev server if in development mode
    mainWindow.loadURL(process.env.WEBPACK_DEV_SERVER_URL);
  } else {
    createProtocol("app");
    // Load the index.html when not in development
    mainWindow.loadURL("app://./index.html");
  }
  /* eslint-ignore-next-line */
  mainWindow.on("ready-to-show", (event) => {
    console.log("MAIN ::: ready-to-show");
    // console.log(" - event:", event);
    // const t = event.target;
    // console.log(" - target:", t);

    mainWindow.show();
    if (isDevelopment) mainWindow.webContents.openDevTools({ mode: "detach" });

    const token = config.get("tmp.token");
    console.log(" - config token: ", token);

    if (token === undefined || token === "") {
      showAuthorizeWindow();
    }
  });

  mainWindow.webContents.on("will-navigate", (event, url) => {
    console.log("MAIN ::: will-navigate handler");
    console.log(" - url: ", url);
    // prevent user from navigating away from application
    event.preventDefault();
  });

  mainWindow.on("closed", () => {
    mainWindow = null;
  });
}

/**
 * Create and show authorize window
 */
function showAuthorizeWindow() {
  console.log("APP ::: showAuthorizeWindow");
  const oauth = subWindows.createAuthWindow(mainWindow)
    .then((data) => {
      console.log("showAuthorizeWindow ::: resolved");
      console.log(" - data:", data);
      config.set("tmp.token", data.token);
      // send new token to vue app
      mainWindow.webContents.send("token-changed", data.token);
    })
    .catch(error => {
      console.log("showAuthorizeWindow ::: rejected");
      console.log(" - error:", error);
      mainWindow.webContents.send("token-error", error);
    });
}

// Quit when all windows are closed.
app.on("window-all-closed", () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  // On macOS it"s common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createMainWindow();
  }
});

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", async () => {
  if (isDevelopment && !process.env.IS_TEST) {
    // Install Vue Devtools
    // Devtools extensions are broken in Electron 6.0.0 and greater
    // See https://github.com/nklayman/vue-cli-plugin-electron-builder/issues/378 for more info
    // Electron will not launch with Devtools extensions installed on Windows 10 with dark mode
    // If you are not using Windows 10 dark mode, you may uncomment these lines
    // In addition, if the linked issue is closed, you can upgrade electron and uncomment these lines
    // try {
    //   await installVueDevtools()
    // } catch (e) {
    //   console.error("Vue Devtools failed to install:", e.toString())
    // }
  }
  createMainWindow();
});

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
  if (process.platform === "win32") {
    process.on("message", data => {
      if (data === "graceful-exit") {
        app.quit();
      }
    });
  } else {
    process.on("SIGTERM", () => {
      app.quit();
    });
  }
}

/**
 * SHOW TWITCH AUTHORIZATION WINDOW
 */
ipcMain.on("show-authorize", () => {
  console.log("IPCMAIN ::: show-authorize");
  showAuthorizeWindow();
});

/**
 * TODO: dummy ipcmain event handler to get around lint error.
 */
ipcMain.on("app-mounted", () => {
  console.log("IPCMAIN ::: app-mounted");
  // console.log(" - data:", event);
  const not1 = new Notification({ title: "New Live Stream 1", body: "Another stream just went live.", tag: "tmplayer" });
  const not2 = new Notification({ title: "New Live Stream 2", body: "Another stream just went live.", tag: "tmplayer" });
  // not1.show();
  // not2.show();
});

/**
 * RENDERER STREAM ITEM "WATCH" CLICK HANDLER
 */
ipcMain.on("stream-watch", (event, user) => {
  console.log("IPCMAIN ::: stream-watch handler");
  console.log(" - user:", user);
  subWindows.createStreamWindow(user);
});

/**
 * RENDERER STREAM ITEM "ONLINE" CLICK HANDLER
 */
ipcMain.on("stream-online", (event, user) => {
  console.log("IPCMAIN ::: stream-online handler");
  const url = twitchApi.getTwitchChannelUrl(user.login);
  shell.openExternal(url);
});

/**
 * RENDERER CHANNEL CLIP ITEM "WATCH" CLICK HANDLER
 */
ipcMain.on("clip-watch", (event, clip) => {
  console.log("IPCMAIN ::: clip-watch handler");
  console.log(" - clip:", clip);
  subWindows.createClipWindow(clip);
});

/**
 * RENDERER CHANNEL CLIP ITEM "ONLINE" CLICK HANDLER
 */
ipcMain.on("clip-online", (event, clipUrl) => {
  console.log("IPCMAIN ::: clip-online handler");
  shell.openExternal(clipUrl);
});

/**
 * RENDERER CHANNEL CLIP ITEM "WATCH" CLICK HANDLER
 */
ipcMain.on("video-watch", (event, video) => {
  console.log("IPCMAIN ::: video-watch handler");
  console.log(" - video:", video);
  subWindows.createVideoWindow(video);
});

/**
 * RENDERER CHANNEL CLIP ITEM "ONLINE" CLICK HANDLER
 */
ipcMain.on("video-online", (event, videoUrl) => {
  console.log("IPCMAIN ::: video-online handler");
  console.log(" - videoUrl:", videoUrl);
  shell.openExternal(videoUrl);
});