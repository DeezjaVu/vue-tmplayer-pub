"use strict";

import getTopGamesCommand from "@/store/commands/GetTopGames.command.js";
import getGamesForIdsCommand from "../commands/GetGamesForIds.command";

// const GET_STREAMS_FOR_GAME = "getStreamsForGame";
const SET_SELECTED_GAME = "setSelectedGame";
const SET_TOP_GAMES = "setTopGames";

/**
 * Games Model
 */
export default {
    state: {
        games: [],
        selectedGame: {}
    },
    mutations: {
        /**
         * 
         * @param {*} context 
         * @param {*} data 
         */
        setTopGames(context, data) {
            console.log("GamesModel ::: mutations ::: setTopGames");
            context.games = data;
        },
        /**
         * 
         * @param {*} context 
         * @param {*} game 
         */
        setSelectedGame(context, game) {
            console.log("StreamsModel ::: mutations ::: setSelectedGame");
            context.selectedGame = game;
        }
    },
    actions: {
        /**
         * 
         */
        async getTopGames({ commit }) {
            console.log("GamesModel ::: actions ::: getTopGames");
            const data = await getTopGamesCommand.execute();
            console.log(' - data:', data);

            commit(SET_TOP_GAMES, data);
            return Promise.resolve();
        },
        /**
         * 
         * @param {*} commit 
         * @param {*} game 
         */
        setSelectedGame({ commit }, game) {
            console.log("StreamsModel ::: actions ::: setSelectedGame");
            commit(SET_SELECTED_GAME, game);
            return Promise.resolve(game);
        },
        /**
         * 
         * @param {*} data 
         */
        async getGamesForIds(context, data) {
            console.log("GamesModel ::: actions ::: getGamesForIds");
            return getGamesForIdsCommand.execute(data);
        },
    },
    modules: {
    }
};