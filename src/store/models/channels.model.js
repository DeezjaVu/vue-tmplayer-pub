"use strict";

import twitchApi from "@/twitch/twitch-api.js";

const SET_FOLLOWED_CHANNELS = "setFollowedChannels";
const SET_CHANNEL_CLIPS = "setChannelClips";
const SET_CHANNEL_VIDEOS = "setChannelVideos";
const SET_SELECTED_CHANNEL = "setSelectedChannel";

/**
 * Channels Model
 */
export default {
    state: {
        channels: [],
        selectedChannel: {},
        clips: [],
        videos: []
    },
    mutations: {
        /**
         * Mutates channels state.
         *
         * @param {*} context
         * @param {*} data
         */
        setFollowedChannels(context, data) {
            console.log("ChannelsModel ::: mutations ::: setFollowedChannels");
            context.channels = data;
        },
        /**
         * Mutates selectedChannel state.
         *
         * @param {*} context
         * @param {*} channel
         */
        setSelectedChannel(context, channel) {
            console.log("ChannelsModel ::: mutations ::: setSelectedChannel");
            context.selectedChannel = channel;
        },
        /**
         * Mutates clips state.
         *
         * @param {*} context
         * @param {*} data
         */
        setChannelClips(context, data) {
            console.log("ChannelsModel ::: mutations ::: setChannelClips");
            context.clips = data;
        },
        /**
         * Mutates videos state.
         * 
         * @param {*} context 
         * @param {*} data 
         */
        setChannelVideos(context, data) {
            console.log("ChannelsModel ::: mutations ::: setChannelVideos");
            context.videos = data;
        }
    },
    actions: {
        /**
         * Fetches and commits user followed channels.
         *
         * @param {*} { commit, dispatch }
         * @param {*} user
         * @returns Promise
         */
        async getFollowedChannels({ commit, dispatch }, user) {
            console.log("ChannelsModel ::: actions ::: getFollowedChannels");
            console.log(" - user:", user);
            // DeezjaVu user id
            const userId = "23773615";
            const headers = twitchApi.headers();
            const apiUrl = `https://api.twitch.tv/helix/users/follows?from_id=${userId}&first=100`;

            try {
                const response = await fetch(apiUrl, { headers: headers });
                console.log(" - response ok:", response.ok);
                const result = await response.json();
                console.log(" - result:", result);
                // TODO: store pagination cursor in state.
                // TODO: store total followed channels in state.
                if (response.ok) {
                    const data = result.data;
                    const users = await dispatch("getFollowedUsers", data);
                    console.log(" - users:", users);
                    commit(SET_FOLLOWED_CHANNELS, data);
                    return Promise.resolve(data);
                } else {
                    return Promise.reject(result);
                }
            } catch (error) {
                return Promise.reject(error);
            }
        },
        /**
         * Commits selected channel in the store"s state.
         * This also resets (clears) channel clips and channel videos.
         *
         * @param {*} { commit }
         * @param {*} channel
         */
        async setSelectedChannel({ commit }, channel) {
            console.log("ChannelsModel ::: actions ::: setSelectedChannel");
            commit(SET_CHANNEL_CLIPS, []);
            commit(SET_CHANNEL_VIDEOS, []);
            commit(SET_SELECTED_CHANNEL, channel);
        },
        /**
         * Fetches and commits clips for the selected channel.
         *
         * @param {*} { commit }
         * @param {*} channelId
         * @returns Promise
         */
        async getChannelClips({ commit, dispatch }, channelId) {
            console.log("ChannelsModel ::: actions ::: getChannelClips");
            const uid = channelId;
            console.log(" - channel id:", uid);

            const headers = twitchApi.headers();
            const apiUrl = `https://api.twitch.tv/helix/clips?broadcaster_id=${uid}&first=100`;

            try {
                const response = await fetch(apiUrl, { headers: headers });
                console.log(" - response ok:", response.ok);
                const result = await response.json();
                console.log(" - result:", result);
                if (!response.ok || (result && result.error)) {
                    return Promise.reject(result);
                } else {
                    const data = result.data;
                    const fullData = await dispatch("getGamesForIds", data);
                    commit(SET_CHANNEL_CLIPS, fullData);
                    return Promise.resolve(fullData);
                }
            } catch (error) {
                return Promise.reject(error);
            }
        },
        /**
         * 
         * @param {*} commit 
         */
        resetChannelClips({ commit }) {
            console.log("ChannelsModel ::: actions ::: resetChannelClips");
            commit(SET_CHANNEL_CLIPS, []);
        },
        /**
         * Fetches and commits videos for the selected channel.
         * 
         * @param {*} { commit }
         * @param {*} channelId
         * @returns Promise
         */
        async getChannelVideos({ commit }, channelId) {
            console.log("ChannelsModel ::: actions ::: getChannelVideos");
            const uid = channelId;

            const headers = twitchApi.headers();
            const apiUrl = `https://api.twitch.tv/helix/videos?user_id=${uid}&first=100`;

            try {
                const response = await fetch(apiUrl, { headers: headers });
                console.log(" - response ok:", response.ok);

                const result = await response.json();
                console.log(" - result:", result);

                if (!response.ok || (result && result.error)) {
                    return Promise.reject(result);
                } else {
                    const data = result.data;
                    commit(SET_CHANNEL_VIDEOS, data);
                    return Promise.resolve(data);
                }
            } catch (error) {
                return Promise.resolve(error);
            }
        },
        /**
         * 
         * @param {*} commit 
         */
        resetChannelVideos({ commit }) {
            console.log("ChannelsModel ::: actions ::: resetChannelVideos");
            commit(SET_CHANNEL_VIDEOS, []);
        },
    },
    getters: {

    },
    modules: {

    }
};