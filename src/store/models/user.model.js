"use strict";

import twitchApi from "@/twitch/twitch-api.js";

import getFollowedUsersCommand from "@/store/commands/GetFollowedUsers.command.js";

const SET_FOLLOWED_USERS = "setFollowedUsers";

export default {
    state: {
        followedUsers: []
    },
    modules: {
    },
    mutations: {
        /**
         * 
         */
        setFollowedUsers(context, data) {
            console.log("UserModel ::: mutations ::: setFollowedUsers");
            context.followedUsers = data;
        }
    },
    actions: {
        /**
         * Retrieve data for authorized user by user id.
         * 
         * @param {*} context 
         * @param {*} uid 
         */
        async getAuthUser() {
            console.log("UserModel ::: actions ::: getAuthUser");

        },
        /**
         * 
         * API https://api.twitch.tv/helix/users
         * 
         * @see https://dev.twitch.tv/docs/api/reference/#get-users
         */
        async getUserForId(context, uid) {
            console.log("UserModel ::: actions ::: getUserForId");
            // asian user id's: 
            //      103825127
            //      103861159
            //      102381201
            const q = "id=" + uid;
            const header = twitchApi.headers();
            console.log(" - fetch headers:", header);
            const apiUrl = 'https://api.twitch.tv/helix/users?' + q;
            const response = await fetch(apiUrl, { headers: header });
            const json = await response.json();
            console.log(' - response ok:', response.ok);
            console.log(' - user:', json);
            if (!response.ok || (json && json.error)) {
                return Promise.reject(json);
            } else {
                // apparenty requesting user info returns an array
                let user = json.data.pop();
                return Promise.resolve(user);
            }
        },
        /**
         * 
         */
        async getFollowedUsers({ commit }, channels) {
            console.log("UserModel ::: actions ::: getFollowedUsers");
            const result = await getFollowedUsersCommand.execute(channels);
            commit(SET_FOLLOWED_USERS, result.data);
            return Promise.resolve(result.data);
        }

    },
    getters: {

    }
};