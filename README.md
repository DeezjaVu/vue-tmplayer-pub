# README #

TMPlayer is a desktop media player for Twitch live streams and videos. Once authorized, the application makes use of the Twitch API to provide quick access to your favorite channels and notifies you when they go live.

## TMPlayer Partial Source Code ##

The code available here is only partial code for demo purposes only. The full application code is kept in a private repo.

### What is this repository for? ###

* This is part of a private repo for demo purposes only.
* Demo video of the actual application:
    [TMPlayer Demo](<https://www.youtube.com/watch?v=s9oHD7LI2QM>)

### How do I get set up? ###

* Since this repo only contains partial code, it is not meant to be set up.
* You can however clone or download the source code for viewing purposes.

### Who do I talk to? ###

* Repo owner or admin
