"use strict";

import twitchApi from "@/twitch/twitch-api.js";

const SET_SEARCH_STREAMS = "setSearchStreams";
const SET_STREAMS_FOR_GAME = "setStreamsForGame";
const SET_LIVE_STREAMS = "setLiveStreams";
const SET_NEW_STREAMS = "setNewStreams";
// const SET_SELECTED_GAME = "setSelectedGame";

/**
 * Streams Model
 */
export default {
    state: {
        streams: [],
        newStreams: [],
        gameStreams: [],
        searchStreams: []
    },
    mutations: {
        /**
         * 
         * @param {*} context 
         * @param {*} streams 
         */
        setLiveStreams(context, streams) {
            console.log("StreamsModel ::: mutations ::: setLiveStreams");
            context.streams = streams;
        },
        /**
         * 
         * @param {*} context 
         * @param {*} streams 
         */
        setNewStreams(context, streams) {
            console.log("StreamsModel ::: mutations ::: setNewStreams");
            context.newStreams = streams;
        },
        /**
         * 
         * @param {*} context 
         * @param {*} data 
         */
        setStreamsForGame(context, data) {
            console.log("StreamsModel ::: mutations ::: setStreamsForGame");
            console.log(' - data:', data);
            context.gameStreams = data;
        },
        /**
         * 
         * @param {*} context 
         * @param {*} data 
         */
        setSearchStreams(context, data) {
            console.log("StreamsModel ::: mutations ::: setSearchStreams");
            context.searchStreams = data;
        },
    },
    actions: {
        /**
         * 
         * @param {*} commit 
         */
        async getLiveStreams({ commit }) {
            console.log("StreamsModel ::: actions ::: getLiveStreams");
            commit(SET_NEW_STREAMS, []);

            // TODO: Use user auth token.
            const header = twitchApi.v5headers();
            // new API (requires a list of user id's)
            // https://api.twitch.tv/helix/streams
            // v5 API
            const apiUrl = "https://api.twitch.tv/kraken/streams/followed";

            try {
                console.log("StreamsModel ::: getLiveStreams ::: try");
                const response = await fetch(apiUrl, { headers: header });
                const result = await response.json();
                console.log(' - result:', result);
                if (response.ok) {
                    // TODO: remove sorting if Twitch fixes their shit.
                    const streams = result.streams.sort((a, b) => (a.viewers - b.viewers) * -1);
                    // const streams = result.streams;
                    // Add first and last streams to new streams list.
                    const newStreams = [streams[0], streams[streams.length - 1]];
                    commit(SET_NEW_STREAMS, newStreams);
                    commit(SET_LIVE_STREAMS, streams);
                    return Promise.resolve(streams);
                } else {
                    return Promise.reject(result);
                }
            } catch (error) {
                console.log("StreamsModel ::: getLiveStreams ::: catch");
                console.log(' - error:', error);
                return Promise.reject(error);
            }
        },
        /**
         * 
         * @param {*} commit
         * @param {*} game 
         */
        async getStreamsForGame({ commit }, gameId) {
            console.log("StreamsModel ::: actions ::: getStreamsForGame");

            const headers = twitchApi.headers();
            const apiUrl = `https://api.twitch.tv/helix/streams?game_id=${gameId}&first=100`;

            try {
                const response = await fetch(apiUrl, { headers: headers });
                console.log(' - response ok:', response.ok);
                const result = await response.json();
                console.log(' - result', result);
                if (response.ok) {
                    const data = result.data;
                    commit(SET_STREAMS_FOR_GAME, data);
                    return Promise.resolve(data);
                } else {
                    return Promise.reject(result.error);
                }
            } catch (error) {
                return Promise.reject(error);
            }
        },
        /**
         * 
         * @param {*} commit 
         */
        resetStreamsForGame({ commit }) {
            console.log("StreamsModel ::: actions ::: resetStreamsForGame");
            commit(SET_STREAMS_FOR_GAME, [])
        },
        /**
         * 
         * @param {*} commit 
         */
        async getSearchStreams({ commit, dispatch }) {
            console.log("StreamsModel ::: actions ::: getSearchStreams");

            // const q = query;
            const headers = twitchApi.headers();
            const apiUrl = "https://api.twitch.tv/helix/streams?language=en&first=100";

            try {
                const response = await fetch(apiUrl, { headers: headers });

                if (response.ok) {
                    const result = await response.json();
                    const streams = result.data;
                    dispatch('getGamesForIds', streams).then(data => {
                        console.log(' - full data:', data);
                        commit(SET_SEARCH_STREAMS, data);
                        Promise.resolve(data);
                    });
                } else {
                    Promise.reject(response);
                }
            } catch (error) {
                Promise.reject(error);
            }
        },
    },
    modules: {
    }
};