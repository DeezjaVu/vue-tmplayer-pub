import twitchApi from "@/twitch/twitch-api.js";

/**
 * Command  &mdash; When executed, retrieves a list of most popular games from the Twitch API.
 */
export default {
    /**
     * Retrieves the top most popular games on Twitch.
     * 
     * @returns Promise. When resolved the payload contains a list of game objects.
     */
    async execute() {
        console.log("GetTopGamesCommand ::: execute");

        const apiUrl = "https://api.twitch.tv/helix/games/top?first=100";
        const headers = twitchApi.headers();

        try {
            const response = await fetch(apiUrl, { headers: headers });
            console.log(' - response ok:', response.ok);

            const result = await response.json();
            console.log(' - result:', result);

            if (response.ok) {
                return Promise.resolve(result.data);
            } else {
                // error: "Unauthorized"
                // message: "Must provide a valid Client-ID or OAuth token"
                // status: 401
                return Promise.reject(result);
            }

        } catch (error) {
            const payload = { error: "Error", message: "Unable to fetch data.", status: 500 };
            console.log(' - reject payload:', payload);
            return Promise.reject(payload);
        }
    }
}