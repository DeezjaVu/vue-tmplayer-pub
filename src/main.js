"use strict";

import Vue from "vue";
import moment from "vue-moment";
import "./plugins/bootstrap-vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import { library } from "@fortawesome/fontawesome-svg-core";
import { faCog, faUserCog, faBars, faHeart, faEye, faHeartBroken, faFilm, faGlobe, faVideo, faFileVideo, faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

// library.add(faCog, faUserCog, faBars, faEye, faHeart, faHeartBroken, faFilm, faGlobe, faVideo, faFileVideo);
library.add(faCog);
library.add(faUserCog);
library.add(faBars);
library.add(faEye);
library.add(faHeart);
library.add(faHeartBroken);
library.add(faFilm);
library.add(faGlobe);
library.add(faVideo);
library.add(faFileVideo);
library.add(faArrowLeft);

Vue.component("font-awesome-icon", FontAwesomeIcon);
Vue.use(moment);

Vue.config.productionTip = false;

import "@/sass/app.scss";

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount("#app");
