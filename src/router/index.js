import Vue from "vue";
import VueRouter from "vue-router";
import Authorize from "@/views/Authorize.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "authorize",
    component: Authorize
  },
  {
    path: "/live-streams",
    name: "live-streams",
    component: () => import(/* webpackChunkName: "live-streams" */ "../views/LiveStreams.vue")
  },
  {
    path: "/favorite-channels",
    name: "favorite-channels",
    component: () => import(/* webpackChunkName: "favorite-channels" */ "../views/FavoriteChannels.vue")
  },
  {
    path: "/channel-videos/:id",
    name: "channel-videos",
    component: () => import(/* webpackChunkName: "channel-videos" */ "../views/ChannelVideos.vue")
  },
  {
    path: "/channel-clips/:id",
    name: "channel-clips",
    component: () => import(/* webpackChunkName: "channel-clips" */ "../views/ChannelClips.vue")
  },
  {
    path: "/popular-games",
    name: "popular-games",
    component: () => import(/* webpackChunkName: "popular-games" */ "../views/PopularGames.vue")
  },
  {
    path: "/popular-games/:id",
    name: "popular-game-streams",
    component: () => import(/* webpackChunkName: "popular-game-streams" */ "../views/PopularGameStreams.vue")
  },
  {
    path: "/search-streams",
    name: "search-streams",
    component: () => import(/* webpackChunkName: "search-streams" */ "../views/SearchStreams.vue")
  }
];

const router = new VueRouter({
  routes
});

router.beforeEach((to, from, next) => {
  console.log("ROUTER ::: beforeEach");
  /* must call `next` */
  next();
});

router.beforeResolve((to, from, next) => {
  console.log("ROUTER ::: beforeResolve");
  /* must call `next` */
  next();
});

export default router;
