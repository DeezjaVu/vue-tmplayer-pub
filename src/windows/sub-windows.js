"use strict";

import { shell, BrowserWindow } from 'electron';
import path from "path";
import twitchApi from "../twitch/twitch-api.js";


const TWITCH_CLIENT_ID = process.env.TWITCH_CLIENT_ID;
const TWITCH_OAUTHURL = process.env.TWITCH_OAUTH_URL;
const TWITCH_REDIRECT_URL = process.env.TWITCH_REDIRECT_URL;
const TWITCH_RESPONSE_TYPE = process.env.TWITCH_RESPONSE_TYPE;
const TWITCH_SCOPES = process.env.TWITCH_SCOPES;

const WINDOW_DEFAULT_WIDTH = 976;
const WINDOW_DEFAULT_HEIGHT = 580;
const WINDOW_MIN_WIDTH = 656;
const WINDOW_MIN_HEIGHT = 420;

const CHAT_DEFAULT_WIDTH = 450;
const CHAT_DEFAULT_HEIGHT = 768;
const CHAT_MIN_WIDTH = 360;
const CHAT_MIN_HEIGHT = 420;

/**
 * 
 */
const subWindows = [];

/**
 * Removes a window from the list of open windows.
 * 
 * @param {*} win - The window to remove.
 * @memberof SubWindows
 */
function removeWindow(win) {
    console.log("SubWindows ::: removeWindow");
    // console.log(' - window:', videoWin);
    let idx = subWindows.indexOf(win);
    if (idx != -1) {
        console.log(" - found matching child window");
        console.log(" - removing child window: ", idx);
        // let win = videoWindows.splice(idx, 1);
        subWindows.splice(idx, 1);
        win = null;
    }
}

let authWindow;

/**
 * Electron window utility module.
 */
export default {

    /**
     * Creates and opens a new window for the given user's live stream.
     * 
     * @param {Object} user - Twitch user object.
     * @memberof SubWindows
     */
    createStreamWindow(user) {
        console.log("SubWindows ::: createStreamWindow");

        let channelName = (user) ? user.login : "esl_csgo";
        console.log(' - channel name: ', channelName);

        let displayName = (user) ? user.display_name : "ESL CSGO";
        console.log(' - display name: ', displayName);

        // size is half of 1920x1080
        // type: 'toolbar', is possible, but no minimize button.
        // kiosk: true, will open the window in fullscreen mode.
        const preload = path.join(__static, "js", "stream-window-preload.js");
        console.log(" - preload path:", preload);

        let streamWin = new BrowserWindow({
            width: WINDOW_DEFAULT_WIDTH,
            height: WINDOW_DEFAULT_HEIGHT,
            minWidth: WINDOW_MIN_WIDTH,
            minHeight: WINDOW_MIN_HEIGHT,
            show: false,
            icon: path.join(__static, "icon.png"),
            title: 'TMPlayer',
            webPreferences: {
                nodeIntegration: false,
                contextIsolation: true,
                // preload: preload,
            }
        });
        streamWin.setMenuBarVisibility(false);

        // let playerUrl = `https://player.twitch.tv/?volume=0.25&!muted&channel=${channel}`;
        let playerUrl = twitchApi.getPlayerUrlForStream(channelName);
        console.log(' - player url: ', playerUrl);

        streamWin.loadURL(playerUrl);
        subWindows.push(streamWin);

        /**
         * 
         */
        streamWin.once('ready-to-show', () => {
            streamWin.show();
            // create chat window for channel
            this.createChatWindow(user, streamWin);
        });

        /**
         * 
         */
        streamWin.webContents.on('page-title-updated', (event, title, explicit) => {
            console.log('SubWindows ::: page-title-updated handler');
            let winTitle = "TMPlayer - " + displayName;
            console.log(' - window title: ', winTitle);
            // change the window title from Twitch's default
            if (title !== winTitle) streamWin.setTitle(winTitle);
        });

        /**
         * 
         */
        streamWin.webContents.on('will-navigate', (event, url) => {
            console.log('SubWindows ::: will-navigate handler');
            console.log(' - initial url: ', playerUrl);
            console.log(' - url: ', url);
            // prevent user from navigating away from video player
            event.preventDefault();
            // create a new window for the given url
            this.createSubWindow(url);
        });

        /**
        * Handle when video window spawns new windows.
        * This is typically initiated by the user clicking 
        * the Twitch logo present in their video player.
        */
        // streamWin.webContents.on('new-window', (event, url, frameName, disp, options) => {
        streamWin.webContents.on('new-window', (event, url) => {
            console.log('SubWindows ::: new-window handler');
            console.log(' - window url: ', url);
            // console.log(' - window disposition: ', disp);
            // console.log(' - window options: ', options);
            //TODO: Check if url is valid http(s) url.
            //* Prevent non Electron window from being created.
            event.preventDefault();
            //* Open url in system browser.
            shell.openExternal(url);
        });

        /**
         * 
         */
        streamWin.webContents.on('did-navigate', (event, url, res, status) => {
            console.log('SubWindows ::: did-navigate handler');
        });

        streamWin.on('closed', (event) => {
            console.log('SubWindows ::: videoWin closed');
            //console.log(" - event: ", event);
            //console.log(" - sender: ", event.sender);
            removeWindow(streamWin);
        });
    },

    /**
     * 
     * @param {string} channelName 
     */
    createChatWindow(user, parentWin) {
        console.log("SubWindows ::: createChatWindow");
        let channelName = (user) ? user.login : "esl_csgo";
        console.log(' - channel name: ', channelName);

        let displayName = (user) ? user.display_name : "ESL CSGO";
        console.log(' - display name: ', displayName);

        let chatUrl = twitchApi.getTwitchChannelChatUrl(channelName);
        console.log(' - chat url:', chatUrl);

        // size is half of 1920x1080
        // type: 'toolbar', is possible, but no minimize button.
        // kiosk: true, will open the window in fullscreen mode.

        let chatWin = new BrowserWindow({
            width: CHAT_DEFAULT_WIDTH,
            height: CHAT_DEFAULT_HEIGHT,
            minWidth: CHAT_MIN_WIDTH,
            minHeight: CHAT_MIN_HEIGHT,
            maximizable: false,
            show: false,
            icon: path.join(__static, "icon.png"),
            title: 'TMPlayer',
            // parent: parentWin,
            webPreferences: {
                nodeIntegration: false,
                contextIsolation: true,
            }
        });
        chatWin.setMenuBarVisibility(false);

        chatWin.loadURL(chatUrl);
        subWindows.push(chatWin);

        /**
         * 
         */
        chatWin.once('ready-to-show', () => {
            chatWin.show();
        });

        /**
         * 
         */
        chatWin.webContents.on('page-title-updated', (event, title, explicit) => {
            console.log('SubWindows ::: page-title-updated handler');
            let winTitle = "TMPlayer Chat - " + displayName;
            console.log(' - window title: ', winTitle);
            // change the window title from Twitch's default
            if (title !== winTitle) chatWin.setTitle(winTitle);
        });

        /**
         * 
         */
        chatWin.webContents.on('will-navigate', (event, url) => {
            console.log('SubWindows ::: will-navigate handler');
            console.log(' - initial url: ', playerUrl);
            console.log(' - url: ', url);
            // prevent user from navigating away from chat.
            event.preventDefault();
            // create a new window for the given url
            this.createSubWindow(url);
        });

        /**
        * Handle when chat window spawns new windows.
        * This is typically initiated by the user clicking a link in the twitch chat. 
        */
        // chatWin.webContents.on('new-window', (event, url, frameName, disp, options) => {
        chatWin.webContents.on('new-window', (event, url) => {
            console.log('SubWindows ::: new-window handler');
            console.log(' - window url: ', url);
            // console.log(' - window disposition: ', disp);
            // console.log(' - window options: ', options);
            //TODO: Check if url is valid http(s) url.
            //* Prevent non Electron window from being created.
            event.preventDefault();
            //* Open url in system browser.
            shell.openExternal(url);
        });

        /**
         * 
         */chatWin;
        chatWin.webContents.on('did-navigate', (event, url, res, status) => {
            console.log('SubWindows ::: did-navigate handler');
        });

        chatWin.on('closed', (event) => {
            console.log('SubWindows ::: chatWin closed');
            //console.log(" - event: ", event);
            //console.log(" - sender: ", event.sender);
            removeWindow(chatWin);
        });
    },

    /**
     * 
     * @param {*} clip 
     */
    createClipWindow(clip) {
        console.log("SubWindows ::: createClipWindow");
        // const clipUrl = clip.embed_url + "&parent=twitch.tv";
        const clipId = clip.id;
        const clipUrl = twitchApi.getPlayerUrlForClip(clipId);
        const userName = clip.broadcaster_name;
        console.log(' - clip: ', clip);
        console.log(' - clip url: ', clipUrl);

        // size is half of 1920x1080
        // type: 'toolbar', is possible, but no minimize button.
        // kiosk: true, will open the window in fullscreen mode.
        let clipWin = new BrowserWindow({
            width: WINDOW_DEFAULT_WIDTH,
            height: WINDOW_DEFAULT_HEIGHT,
            minWidth: WINDOW_MIN_WIDTH,
            minHeight: WINDOW_MIN_HEIGHT,
            show: false,
            icon: path.join(__static, 'icon.png'),
            title: `TMPlayer - ${userName}`,
            webPreferences: {
                nodeIntegration: false,
                contextIsolation: true
            }
        });
        clipWin.setMenuBarVisibility(false);

        clipWin.loadURL(clipUrl);
        subWindows.push(clipWin);

        /**
         * 
         */
        clipWin.webContents.on('page-title-updated', (event, title, explicit) => {
            console.log('SubWindows ::: page-title-updated handler');
            let winTitle = `TMPlayer - ${userName}`;
            console.log(' - window title: ', winTitle);
            console.log(' - arg title: ', title);
            console.log(' - arg explicit: ', explicit);
            // change the window title from Twitch's default
            if (title !== winTitle) clipWin.setTitle(winTitle);
        });

        /**
         * 
         */
        clipWin.webContents.on('will-navigate', (event, url) => {
            console.log('SubWindows ::: will-navigate handler');
            console.log(' - initial url: ', clipUrl);
            console.log(' - url: ', url);
            // prevent user from navigating away from video player
            event.preventDefault();
            // create a new window for the given url
            this.createSubWindow(url);
        });

        /**
        * Handle when video window spawns new windows.
        * This is typically initiated by the user clicking 
        * the Twitch logo present in their video player.
        */
        clipWin.webContents.on('new-window', (event, url, frameName, disp, options) => {
            console.log('SubWindows ::: new-window handler');
            console.log(' - window url: ', url);
            console.log(' - window disposition: ', disp);
            console.log(' - window options: ', options);
            // Prevent non Electron window from being created.
            event.preventDefault();
            //INFO: if user clicked another clip, open it in the same window, otherwise open url in system browser.
            const isClip = (url.indexOf("clips.twitch.tv") !== -1);
            console.log(' - url is clip:', isClip);
            if (isClip) {
                // Get the new clip title
                const clipTitle = url.split("/").pop().split("?").shift();
                // Construct new clip embed url
                const newClipUrl = `https://clips.twitch.tv/embed?clip=${clipTitle}`;
                console.log(' - new clip url:', newClipUrl);
                //* Load clip in existing window.
                clipWin.loadURL(newClipUrl);
            } else {
                //* Open url in system browser.
                shell.openExternal(url);
            }
        });

        /**
         * 
         */
        clipWin.webContents.on('did-navigate', (event, url, res, status) => {
            console.log('SubWindows ::: did-navigate handler');
        });

        /**
         * 
         */
        clipWin.once('ready-to-show', () => {
            clipWin.show();
        });

        clipWin.on('closed', (event) => {
            console.log('SubWindows ::: clipWin closed');
            removeWindow(clipWin);
        });
    },

    /**
     * Creates and opens a new window for the given channel video.
     * 
     * @param {Object} video - Channel video object.
     * @memberof SubWindows
     */
    createVideoWindow(video) {
        console.log("SubWindows ::: createVideoWindow");
        const videoId = video.id;
        // const playerUrl = `https://player.twitch.tv/?enableExtensions=false&muted=false&player=popout&&video=${videoId}&volume=0.20`;
        const playerUrl = twitchApi.getPlayerUrlForVideo(videoId);
        console.log(' - popup player url:', playerUrl);

        // size is half of 1920x1080
        // type: 'toolbar', is possible, but no minimize button.
        // kiosk: true, will open the window in fullscreen mode.
        let videoWin = new BrowserWindow({
            width: WINDOW_DEFAULT_WIDTH,
            height: WINDOW_DEFAULT_HEIGHT,
            minWidth: WINDOW_MIN_WIDTH,
            minHeight: WINDOW_MIN_HEIGHT,
            show: false,
            icon: path.join(__static, 'icon.png'),
            title: 'TMPlayer',
            webPreferences: {
                nodeIntegration: false,
                contextIsolation: true
            }
        });

        videoWin.setMenuBarVisibility(false);

        videoWin.loadURL(playerUrl);
        subWindows.push(videoWin);

        /**
         * 
         */
        videoWin.once('ready-to-show', () => videoWin.show());

        /**
         * 
         */
        videoWin.webContents.on('page-title-updated', (event, title, explicit) => {
            console.log('SubWindows ::: page-title-updated handler');
            let winTitle = "TMPlayer - " + video.user_name;
            console.log(' - window title: ', winTitle);
            // change the window title from Twitch's default
            if (title !== winTitle) videoWin.setTitle(winTitle);
        });

        /**
          * 
          */
        videoWin.webContents.on('will-navigate', (event, url) => {
            console.log('SubWindows ::: will-navigate handler');
            console.log(' - initial url: ', playerUrl);
            console.log(' - url: ', url);
            // prevent user from navigating away from video player
            event.preventDefault();
            // create a new window for the given url
            this.createSubWindow(url);
        });

        /**
        * Handle when video window spawns new windows.
        * This is typically initiated by the user clicking 
        * the Twitch logo present in their video player.
        */
        videoWin.webContents.on('new-window', (event, url, frameName, disp, options) => {
            console.log('SubWindows ::: new-window handler');
            console.log(' - window url: ', url);
            console.log(' - window disposition: ', disp);
            console.log(' - window options: ', options);
            //TODO: Check if url is valid http(s) url.
            //* Prevent non Electron window from being created.
            event.preventDefault();
            //* Open url in system browser.
            shell.openExternal(url);
        });

        videoWin.on('closed', (event) => {
            console.log('SubWindows ::: videoWin closed');
            //console.log(" - event: ", event);
            //console.log(" - sender: ", event.sender);
            removeWindow(videoWin);
        });
    },

    /**
     * Creates and opens a modal window for Twitch user authorization.
     * 
     * @param {*} parentWindow - The main process window.
     * @memberof SubWindows
     */
    createAuthWindow(parentWindow) {
        console.log("SubWindows ::: createOAuthWindow");
        // console.log(' - parent window:', parentWindow);
        // previous bearer token: mp9dg554ta4e8560lgjadii13q4gzc
        // latest bearer token: 8cebrlczuc8t1eqtchlp3c1puh38ev

        // const oauthPromise = new Promise();

        const oauthUrl = TWITCH_OAUTHURL;
        const clientId = TWITCH_CLIENT_ID;
        const redirectUrl = TWITCH_REDIRECT_URL;
        const responseType = TWITCH_RESPONSE_TYPE;
        const scopes = TWITCH_SCOPES;
        const state = new Date().getTime();
        console.log(' - state:', state);

        let tmpOAuthUrl = oauthUrl;
        tmpOAuthUrl += `?client_id=${clientId}`;
        tmpOAuthUrl += `&redirect_uri=${redirectUrl}`;
        tmpOAuthUrl += `&response_type=${responseType}`;
        tmpOAuthUrl += `&scope=${scopes}`;
        tmpOAuthUrl += `&state=${state}`;
        tmpOAuthUrl += "&force_verify=true";

        console.log(' - tmplayer OAuth url:', tmpOAuthUrl);
        // 640 x 720
        authWindow = new BrowserWindow({
            width: 656,
            height: 754,
            minWidth: 516,
            minHeight: 654,
            type: 'toolbar',
            parent: parentWindow,
            modal: true,
            minimizable: false,
            maximizable: false,
            closable: false,
            show: false,
            webPreferences: {
                nodeIntegration: false,
                contextIsolation: true
            }
        });

        authWindow.setMenuBarVisibility(false);
        authWindow.loadURL(tmpOAuthUrl);

        authWindow.once('ready-to-show', () => {
            console.log('SubWindows ::: authWindow ready-to-show handler');
            authWindow.show();
        });

        authWindow.once('closed', (event) => {
            console.log("SubWindows ::: authWindow closed handler");
            authWindow = null;
        });

        return new Promise((resolve, reject) => {
            authWindow.webContents.on('did-navigate', (event, contentUrl, responseCode, status) => {
                console.log('SubWindows ::: authWindow did-navigate handler');
                // event Event - url String
                // httpResponseCode Integer - -1 for non HTTP navigations
                // httpStatusText String - empty for non HTTP navigations
                //console.log(' - event: ', event);
                console.log(' - url: ', contentUrl);
                console.log(' - httpResponseCode: ', responseCode);
                console.log(' - httpStatusText: ', status);

                let navigateUrl = new URL(contentUrl);
                console.log(' - navigate url:', navigateUrl);
                // check if we're at the app's OAuth redirect url
                // TODO: OAuth -> verify returned state matches sent state.
                if (navigateUrl && navigateUrl.hostname === "sites.google.com") {
                    console.log("We may have logged in.");
                    let urlHash = navigateUrl.hash;
                    let urlQuery = navigateUrl.searchParams;
                    if (urlHash) {
                        let tokenHash = urlHash.split("&");
                        console.log(' - token hash:', tokenHash);
                        let token = tokenHash.shift().split("=").pop();
                        console.log(' - token:', token);
                        // windowEmitter.emit("oauth-changed", { token: token });
                        authWindow.setClosable(true);
                        authWindow.close();

                        resolve({ token: token });

                    } else if (urlQuery.has("error")) {
                        console.log(' - error:', urlQuery.get("error"));
                        console.log(' - desciption:', urlQuery.get("error_description"));
                        // go back to start.
                        authWindow.loadURL(tmpOAuthUrl);
                    } else {
                        console.log("OAuth Promise - Unknown error");
                        // Something went wrong
                        reject({ error: "Unknown Error", message: "Authorization failed.", status: 401 });
                    }
                } else if (responseCode !== 200) {
                    reject({ error: "Error", message: "Authorization failed.", status: responseCode });
                }

            });
        });

    },

    /**
     * 
     */
    createSubWindow: (url) => {
        console.log("SubWindows ::: createSubWindow");
        let childWin = new BrowserWindow({
            width: 976,
            height: 578,
            type: 'toolbar',
            show: false,
            //top: mainWindow,
            title: 'Twitch',
            webPreferences: {
                nodeIntegration: false,
                contextIsolation: true
            }
        });
        childWin.setMenuBarVisibility(false);
        // Load url into child window.
        childWin.loadURL(url);
        // When ready, show the new window.
        childWin.once('ready-to-show', () => childWin.show());
    }

};

