import twitchApi from "@/twitch/twitch-api.js";

/**
 * Command &mdash; When executed, fetches game details from the Twitch API.
 */
export default {

    /**
     * Retrieves game data from Twitch API.
     * 
     * This modifies the received data and returns it when the Promise is resolved.
     * 
     * @param {*} data A list (Array) of game objects for which to retrieve detailed info.
     * @returns Promise. When resolved the original list of game objects with detailed game info added is returned as payload.
     */
    async execute(data) {
        console.log("GetGamesForIdsCommand ::: execute");
        //* SHOCKER: Twitch data is not consistent, meaning we need to fix that.
        const q = data.reduce((ids, s) => {
            if (s.hasOwnProperty("game_id") && s.game_id !== "0" && s.game_id !== "") {
                if (!ids.includes(s.game_id)) ids += ("&id=" + s.game_id);
            }
            return ids;
        }, "");

        // console.log(" - query: ", q);

        const headers = twitchApi.headers();

        const apiUrl = `https://api.twitch.tv/helix/games?${q}`;
        console.log(" - api url:", apiUrl);

        const response = await fetch(apiUrl, { headers: headers });
        const result = await response.json();
        console.log(" - result:", result);

        if (response.ok) {
            const games = result.data;
            // Loop through streams data and find matching id in games data.
            // When a match is found it is added to the stream data (stream.game).
            // If no match is found, a default object is added to prevent null pointer exceptions when accessing the data.
            data.forEach(s => s.game = games.find(g => g.id === s.game_id) || { id: "", name: "", box_art_url: "" });
            return Promise.resolve(data);
        } else {
            return Promise.reject(response);
        }
    }
};