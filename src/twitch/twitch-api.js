"use strict";

/**
 * 
 */
const CLIENT_ID = process.env.VUE_APP_TWITCH_CLIENT_ID;

// TODO: Replace hardcoded oauth token with actual token.
const USER_OAUTH_TOKEN = "";

/**
 * Kraken base url (v5 API).
 */
const TWITCH_KRAKEN_URI = "https://api.twitch.tv/kraken";

/**
 * Helix base url (latest API).
 */
const TWITCH_HELIX_URI = "http://api.twitch.tv/helix";

/**
 * Live stream player popout url.
 * Replace '{channel}' with the channel name.
 * 
 * https://player.twitch.tv/?enableExtensions=false&parent=www.twitch.tv&parent=player.twitch.tv&volume=0.20&!muted&channel={channel}
 */
const TWITCH_STREAM_PLAYER_URL = "https://player.twitch.tv/?enableExtensions=false&parent=twitch.tv&volume=0.20&!muted&channel={channel}";

/**
 * Video player popout url.
 * Replace '{videoId}' with the id of the video.
 * 
 * https://player.twitch.tv/?enableExtensions=false&muted=false&parent=www.twitch.tv&parent=player.twitch.tv&player=popout&video={videoId}&volume=0.20
 */
const TWITCH_VIDEO_PLAYER_URL = "https://player.twitch.tv/?enableExtensions=false&muted=false&parent=twitch.tv&player=popout&video={videoId}&volume=0.20";

/**
 * Clip player base url.
 * Replace '{clipId}' with the id of the video clip.
 * 
 * https://clips.twitch.tv/embed?enableExtensions=false&muted=false&parent=twitch.tv&player=popout&clip={clipId}&volume=0.20
 */
const TWITCH_CLIP_PLAYER_URL = "https://clips.twitch.tv/embed?enableExtensions=false&autoplay=true&muted=false&parent=twitch.tv&player=popout&clip={clipId}&volume=0.20";

/**
 * Twitch channel url.
 * Replace '{channel}' with the channel name.
 */
const TWITCH_CHANNEL_URL = "https://twitch.tv/{channel}";

/**
 * Twitch channel chat popout url.
 * 
 * @example
 * https://www.twitch.tv/popout/mattypocket/chat?popout=
 */
const TWITCH_CHAT_POPOUT_URL = "https://www.twitch.tv/popout/{channel}/chat?popout=";

/**
 * 
 */
export default {
    /**
     * Twitch headers required by the (newer) Helix API.
     */
    headers() {
        return {
            "Accept": "application/json",
            "Authorization": `Bearer ${USER_OAUTH_TOKEN}`,
            "Content-Type": "application/json",
            "Client-ID": CLIENT_ID
        };
    },

    /**
     * Twitch v5 headers required by the (older) Kraken API.
     */
    v5headers() {
        return {
            "Accept": "application/vnd.twitchtv.v5+json",
            "Authorization": `OAuth ${USER_OAUTH_TOKEN}`,
            "Client-ID": CLIENT_ID,
            "Content-Type": "application/json"
        };
    },

    /**
     * 
     */
    getKrakenBaseUri() {
        return TWITCH_KRAKEN_URI;
    },

    /**
     * 
     */
    getHelixBaseUri() {
        return TWITCH_HELIX_URI;
    },

    /**
     * 
     * @param {string} channelName Name of the channel for which to retrieve the video player url.
     * @returns {string} The video player (popup) url of the given channel.
     */
    getPlayerUrlForStream(channelName) {
        return TWITCH_STREAM_PLAYER_URL.replace("{channel}", channelName);
    },

    /**
     *
     * @param {*} videoId
     */
    getPlayerUrlForVideo(videoId) {
        return TWITCH_VIDEO_PLAYER_URL.replace("{videoId}", videoId);
    },

    /**
     *
     * @param {*} clipId
     */
    getPlayerUrlForClip(clipId) {
        return TWITCH_CLIP_PLAYER_URL.replace("{clipId}", clipId);
    },

    /**
     * 
     * @param {string} channelName  Name of the channel for which to retrieve the twitch.tv url.
     * @returns {string} The twitch.tv url of the given channel.
     */
    getTwitchChannelUrl(channelName) {
        return TWITCH_CHANNEL_URL.replace('{channel}', channelName);
    },

    /**
     * 
     * @param {string} channelName Name of the Twitch channel.
     * @returns {string} The twitch.tv chat popout url for the given channel.
     */
    getTwitchChannelChatUrl(channelName) {
        return TWITCH_CHAT_POPOUT_URL.replace('{channel}', channelName);
    }
};
