import twitchApi from "@/twitch/twitch-api.js";

/**
 * Command  &mdash; When executed, retrieves a list of users from the Twitch API.
 */
export default {
    /**
     * 
     * @param {*} channels A list of Twitch channels for which to retrieve detailed user info.
     * @returns Promise.
     */
    async execute(channels) {
        console.log("GetFollowedUsersCommand ::: execute");
        const ids = channels.map((channel) => channel.to_id);
        // console.log(ids);

        const q = "id=" + ids.join("&id=");
        // console.log(" - query user ids:", q);

        const header = twitchApi.headers();
        //warn: the following url will trigger an error.
        // const apiUrl = `https://api.twitch.tv/helix/losers?${q}`;
        const apiUrl = `https://api.twitch.tv/helix/users?${q}`;

        try {
            const response = await fetch(apiUrl, { headers: header });
            console.log(" - response ok:", response.ok);

            const result = await response.json();
            console.log(" - result:", result);

            if (response.ok) {
                return Promise.resolve(result);
            } else {
                // return Promise.reject(result);
                throw result;
            }
        } catch (error) {
            return Promise.reject(error);
        }
    }
};